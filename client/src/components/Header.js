import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <header>
      <h1>Player Management App</h1>
      <hr />
      <div className="links">
        <NavLink to="/" className="link" activeclassname="active">
          Players List
        </NavLink>
        <NavLink to="/add" className="link" activeclassname="active">
          Add Player
        </NavLink>
      </div>
    </header>
  );
};

export default Header;
