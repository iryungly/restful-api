import React, { useContext, useState } from 'react';
import _ from 'lodash';
import Player from './Player';
import PlayersContext from '../context/PlayersContext';

const PlayerList = () => {
  const { players, setPlayers } = useContext(PlayersContext);
  const [ dataCari, setCari ] = useState([]);
  const [ filterCari, setFilterCari ] = useState("");
  var dataOld = players;
  const handleRemovePlayer = (id) => {
    setPlayers(players.filter((player) => player.id !== id));
    dataOld = players;
  };
  var cari = "";
  const searchPlayer = (e) => {
    cari = e.target.value;
    if(cari != ''){
    var data = players.filter((player) => { return player.username.includes(cari) || player.email.includes(cari) || player.experience.includes(cari) || player.lvl.includes(cari)});
      setFilterCari(cari);
      setCari(data);
    }else{
      setCari([]);
      setFilterCari("");
    }
  }
  return (
    <div>
      <div className="player-list">
        <p className="message">Please input to find data</p>
        <input type="text" className="input-control" placeholder="Search data" onChange={searchPlayer} />
        {
        !_.isEmpty(dataCari) && !_.isEmpty(filterCari) ? (
          dataCari.map((player) => (
            <Player key={player.id} {...player} handleRemovePlayer={handleRemovePlayer} />
          ))
        ) : 
        _.isEmpty(dataCari) && !_.isEmpty(filterCari) ? 
        <p className="message">Data not found</p>
        :
        !_.isEmpty(players) ? (
          players.map((player) => (
            <Player key={player.id} {...player} handleRemovePlayer={handleRemovePlayer} />
          ))
        ) : (
          <p className="message">No players available. Please add some players.</p>
        )}
      </div>
    </div>
  );
};

export default PlayerList;
