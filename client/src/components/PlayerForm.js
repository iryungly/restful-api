import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';

const PlayerForm = (props) => {
  const [players, setPlayers] = useState(() => {
    return {
      username: props.player ? props.player.username : '',
      email: props.player ? props.player.email : '',
      experience: props.player ? props.player.experience : '',
      lvl: props.player ? props.player.lvl : '',
    };
  });

  const [errorMsg, setErrorMsg] = useState('');
  const { username, email, experience, lvl } = players;

  const handleOnSubmit = (event) => {
    event.preventDefault();
    const values = [username, email, experience, lvl];
    let errorMsg = '';

    const allFieldsFilled = values.every((field) => {
      const value = `${field}`.trim();
      return value !== '' && value !== '0';
    });
    console.log(allFieldsFilled, 'allFieldsFilled')
    if (allFieldsFilled) {
      const param = {
        id: uuidv4(),
        username,
        email,
        experience,
        lvl
      };
      props.handleOnSubmit(param);
    } else {
      errorMsg = 'Please fill out all the fields.';
    }
    setErrorMsg(errorMsg);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case 'experience':
        if (value === '' || parseInt(value) === +value) {
          setPlayers((prevState) => ({
            ...prevState,
            [name]: value
          }));
        }
        break;
      case 'lvl':
        if (value === '' || value.match(/^\d{1,}(\.\d{0,2})?$/)) {
          setPlayers((prevState) => ({
            ...prevState,
            [name]: value
          }));
        }
        break;
      default:
        setPlayers((prevState) => ({
          ...prevState,
          [name]: value
        }));
    }
  };

  return (
    <div className="main-form">
      {errorMsg && <p className="errorMsg">{errorMsg}</p>}
      <Form onSubmit={handleOnSubmit}>
        <Form.Group controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="username"
            value={username}
            placeholder="Enter username"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="email"
            value={email}
            placeholder="Enter email"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="experience">
          <Form.Label>Experience</Form.Label>
          <Form.Control
            className="input-control"
            type="number"
            name="experience"
            value={experience}
            placeholder="Enter experience"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="lvl">
          <Form.Label>Level</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="lvl"
            value={lvl}
            placeholder="Enter lvl"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit" className="submit-btn">
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default PlayerForm;
