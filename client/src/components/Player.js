import React from 'react';
import { Button, Card } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const Player = ({
  id,
  username,
  email,
  experience,
  lvl,
  date,
  handleRemovePlayer
}) => {
  const navigate = useNavigate();

  return (
    <Card style={{ width: '18rem' }} className="player">
      <Card.Body>
        <Card.Title className="player-title">{username}</Card.Title>
        <div className="player-details">
          <div>Email: {email}</div>
          <div>Experience: {experience} </div>
          <div>Level: {lvl} </div>
        </div>
        <Button variant="primary" onClick={() => navigate(`/edit/${id}`)}>
          Edit
        </Button>{' '}
        <Button variant="danger" onClick={() => handleRemovePlayer(id)}>
          Delete
        </Button>
      </Card.Body>
    </Card>
  );
};

export default Player;
