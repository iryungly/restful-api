import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Header from '../components/Header';
import AddPlayer from '../components/AddPlayer';
import PlayersList from '../components/PlayersList';
import useLocalStorage from '../hooks/useLocalStorage';
import EditPlayer from '../components/EditPlayer';
import BooksContext from '../context/PlayersContext';

const AppRouter = () => {
  const [players, setPlayers] = useLocalStorage('players', []);

  return (
    <BrowserRouter>
      <div>
        <Header />
        <div className={"main-content"}>
          <BooksContext.Provider value={{ players, setPlayers }}>
            <Routes>
              <Route element={<PlayersList/>} path="/" />
              <Route element={<AddPlayer/>} path="/add" />
              <Route element={<EditPlayer/>} path="/edit/:id" />
              <Route element={() => <Navigate to="/" />} />
            </Routes>
          </BooksContext.Provider>
        </div>
      </div>
    </BrowserRouter>
  );
};

export default AppRouter;
